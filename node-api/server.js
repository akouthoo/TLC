// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express      = require('express');        // call express
var app          = express();                 // define our app using express
var bodyParser   = require('body-parser');
var url          = require('url');
var ArrayUtility = require('./app/ArrayUtility');
var au           = new ArrayUtility();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

var mongoose = require('mongoose');

mongoose.connect('localhost:27017', {db: { tlc: true } });

var Sequence     = require('./app/models/sequence');
var UserEventRef = require('./app/models/usereventref');
var UserTaskRef  = require('./app/models/usertaskref');
var User         = require('./app/models/user');
var Task         = require('./app/models/task');
var Event        = require('./app/models/event');
var Link         = require('./app/models/link')

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

// on routes that end in /bears
// ----------------------------------------------------
router.route('/create/user')
	.post(function(req, res) {
		if(req.body.username && req.body.password && req.body.type) {
			User.find({username: req.body.username}).exec(function(err, result) {
				if(result.size > 0) {
					res.send(uid: result[0].uid);
				}
				else {
					Sequence.find({name: 'uids'}).exec(function(err, currUID) {
						var uid = currUID[0].seq + 1,
							user = new User();

						user.uid = uid;
						user.username = req.body.username;
						user.password = req.body.password;
						user.type = req.body.type;
						user.save(function(err) {
							if(err)
								res.send(err);

							Sequence.update({name: 'uids'}, {$inc: {seq: 1}}, function(err) {
								if(err)
									res.send(err);

								res.send(uid: user.uid);
							});
						});
					});
				}
			});
		}
		else {
			res.send(400);
		}
	});

router.route('/create/event')
    .post(function(req, res) {
		if(req.body.title && req.body.uid && req.body.datetime) {
			Sequence.find({name: 'eids'}).exec(function(err, currEID) {
				if(err)
					res.send(err);

				var eid = currEID[0].seq + 1,
					usereventref = new UserEventRef();

				usereventref.tid = req.body.uid;
				usereventref.eid = eid;
				usereventref.save(function(err) {
					if(err)
						res.send(err);

					var event = new Event();

					event.eid = eid;
					event.title = req.body.title;
					event.desc = req.body.desc || '';
					event.datetime = req.body.datetime;
					event.save(function(err) {
						if(err)
							res.send(err);

						Sequence.update({name: 'eids'}, {$inc: {seq: 1}}, function(err) {
							if(err)
								res.send(err);

							res.send(200);
						});
					});
				});
			});
        }
		else {
			res.send(400);
		}
    });

router.route('/create/task')
    .post(function(req, res) {
		if(req.body.title && req.body.landlord_uid && req.body.tennant_uid && req.body.datetime) {
			Sequence.find({name: 'tids'}).exec(function(err, currTID) {
				if(err)
					res.send(err);

				var tid = currTID[0].seq + 1,
					usertaskref = new UserTaskRef();

				usertaskref.landlord_uid = req.body.landlord_uid;
				usertaskref.tennant_uid = req.body.tennant_uid;
				usertaskref.tid = tid;
				usertaskref.save(function(err) {
					if(err)
						res.send(err);

					var task = new Task();

					task.tid = tid;
					task.title = req.body.title;
					task.desc = req.body.desc || '';
					task.datetime = req.body.datetime;
					task.completion = false;
					task.save(function(err) {
						if(err)
							res.send(err);

						Sequence.update({name: 'tids'}, {$inc: {seq: 1}}, function(err) {
							if(err)
								res.send(err);

							res.send(200);
						});
					});
				});
			});
        }
		else {
			res.send(400);
		}
    });

router.route('/create/link')
    .post(function(req, res) {
		if(req.body.landlord_uid && req.body.tennant_uid) {
			var link = new Link();

			link.landlord_uid = req.body.landlord_uid;
			link.tennant_uid = req.body.tennant_uid;

			link.save(function(err) {
				if(err)
					res.send(err);

				res.send(200);
			});
		}
		else {
			res.send(400);
		}
	});

router.route('/update/task')
	.post(function(req, res) {
		if(req.body.landlord_uid && req.body.tennant_uid && req.body.completion) {
			UserTaskRef.find({landlord_uid: req.body.landlord_uid, tennant_uid: req.body.tennant_uid}).exec(function(err, tasks) {
				if(err)
					res.send(err);

				Task.update({tid: tasks[0].tid}, {$set: {completion: req.body.completion}}, function(err) {
					if(err)
						res.send(err);

					res.send(200);
				});
			});
		}
		else {
			res.send(400);
		}
	});

router.route('/get/tennants')
	.get(function(req, res) {
		var queryData = url.parse(req.url, true).query,
			finalTennants = [];
		if(queryData.uid) {
			Link.find({landlord_uid: queryData.uid}).exec(function(err, pairs) {
				if(err)
					res.send(err);

				au.foreach(pairs, function(pair, done) {
					User.find({uid: pair.tennant_uid}).exec(function(err2, tennant) {
						if(err2)
							console.log('ERR: ' + err2);

						finalTennants.push(tennant[0].username);
						done();
					});
				}, function() {
					res.send(JSON.stringify(finalTennants));
				});
			});
		}
	});

router.route('/get/tasks')
	.get(function(req, res) {
		var queryData = url.parse(req.url, true).query,
			finalTasks = [];
		if(queryData.landlord_uid && queryData.tennant_uid) {
			UserTaskRef.find({landlord_uid: queryData.landlord_uid, tennant_uid: queryData.tennant_uid}).exec(function(err, taskRefs) {
				if(err)
					res.send(err);

				au.foreach(taskRefs, function(taskRef, done) {
					Task.find({tid: taskRef.tid}).exec(function(err2, task) {
						if(err2)
							console.log('ERR: ' + err2);

						finalTasks.push(task[0]);
						done();
					});
				}, function() {
					res.send(JSON.stringify(finalTasks));
				});
			});
		}
	});

router.route('/get/events')
	.get(function(req, res) {
		var queryData = url.parse(req.url, true).query,
			finalEvents = [];
		if(queryData.landlord_uid && queryData.tennant_uid) {
			UserEventRef.find({landlord_uid: queryData.landlord_uid, tennant_uid: queryData.tennant_uid}).exec(function(err, eventRefs) {
				if(err)
					res.send(err);

				au.foreach(eventRefs, function(eventRef, done) {
					Event.find({eid: eventRef.eid}).exec(function(err2, event) {
						if(err2)
							console.log('ERR: ' + err2);

						finalEvents.push(event[0]);
						done();
					});
				}, function() {
					res.send(JSON.stringify(finalEvents));
				});
			});
		}
	});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
