
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var TaskSchema   = new Schema({
    tid: Number,
    title: String,
    desc: String,
    image: String,
    completion: Boolean
});

module.exports = mongoose.model('Task', TaskSchema);
