var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserEventRefSchema   = new Schema({
    landlord_uid: Number,
    tennant_uid: Number,
    eid: Number
});

module.exports = mongoose.model('UserEventRef', UserEventRefSchema);
