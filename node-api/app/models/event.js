
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var EventSchema   = new Schema({
    eid: Number,
    title: String,
    desc: String,
    datetime: Number
});

module.exports = mongoose.model('Event', EventSchema);
