
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
    uid: Number,
    username: String,
    password: String,
    type: String
});

module.exports = mongoose.model('User', UserSchema);
