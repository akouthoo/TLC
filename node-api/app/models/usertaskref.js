var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserTaskRefSchema   = new Schema({
    landlord_uid: Number,
    tennant_uid: Number,
    tid: Number
});

module.exports = mongoose.model('UserTaskRef', UserTaskRefSchema);
