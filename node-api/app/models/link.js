
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var LinkSchema   = new Schema({
    landlord_uid: Number,
    tennant_uid: Number
});

module.exports = mongoose.model('Link', LinkSchema);
