
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var SequenceSchema   = new Schema({
    name: String,
    seq: Number
});

module.exports = mongoose.model('Sequence', SequenceSchema);
