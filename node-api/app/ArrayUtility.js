/**
 * @class Module.Models.ArrayUtility
 * Utility class to emulate for each loops in an asynchronous system.
 *
 * @constructor
 * @returns {ArrayUtility}
 */
ArrayUtility = function () {
    return this;
};

ArrayUtility.prototype = {};

/**
 * @method nextIndex
 * @param {Number} count Tracks current position in array
 * @param {Array} array Array containing the values to iterate over
 * @param {Function} runFn Function to run
 * @param {Function} next The callback function
 */
ArrayUtility.prototype.nextIndex = function(count, array, runFn, next){
    var ArrayUtility = this;

    if(count >= array.length){
        next();
    }
    else{
        runFn(array[count], function(){
            count = count + 1;
            ArrayUtility.nextIndex(count, array, runFn, next);
        });
    }
};

/**
 * @method foreach
 * @param {Array} array Array containing the values to iterate over
 * @param {Function} runFn Function to run
 * @param {Function} next The callback function
 */
ArrayUtility.prototype.foreach = function (array, runFn, next) {
    if (!array){
        next();
    }
    else {
        this.nextIndex(0, array, runFn, next);
    }
};

module.exports = ArrayUtility;
