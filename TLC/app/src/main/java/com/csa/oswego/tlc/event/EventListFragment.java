package com.csa.oswego.tlc.event;


import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.joda.time.DateTime;

import java.util.ArrayList;

public class EventListFragment extends ListFragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        final ArrayList<Event> events = new ArrayList<>();
        DateTime time = DateTime.now();

        for (int i = 0; i < 10; i++)
        {
            String title = String.format("Event %d", i);
            String description = String.format("Description of Item %d", i);

            events.add(new Event(i, title, description, time));
        }

        setListAdapter(new EventAdapter(getActivity(), events));

        return view;
    }
}