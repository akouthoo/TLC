package com.csa.oswego.tlc;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import com.viewpagerindicator.TitlePageIndicator;


public class ActivityHome extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ViewPager pager = (ViewPager)findViewById(R.id.viewpager_tenant);
        pager.setAdapter(new TenantPagerAdapter(getSupportFragmentManager()));

        TitlePageIndicator titleIndicator = (TitlePageIndicator)findViewById(R.id.viewpager_header_tenant);
        titleIndicator.setViewPager(pager);
    }
}