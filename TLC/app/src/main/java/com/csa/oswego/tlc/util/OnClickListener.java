package com.csa.oswego.tlc.util;


import android.view.View;

public interface OnClickListener
{
    void onClick(View view);
}