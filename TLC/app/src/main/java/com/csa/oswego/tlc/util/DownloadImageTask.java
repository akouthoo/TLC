package com.csa.oswego.tlc.util;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap>
{
    Consumer<Bitmap> bitmapConsumer;

    public DownloadImageTask(Consumer<Bitmap> bitmapConsumer)
    {
        this.bitmapConsumer = bitmapConsumer;
    }

    protected Bitmap doInBackground(String... urls)
    {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try
        {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        }
        catch (Exception e)
        {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result)
    {
        bitmapConsumer.consume(result);
    }

    public static class BitmapContainer
    {
        public Bitmap bitmap;
    }
}