package com.csa.oswego.tlc.util;

public interface Consumer<T>
{
    void consume(T t);
}