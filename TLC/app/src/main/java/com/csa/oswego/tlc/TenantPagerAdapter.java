package com.csa.oswego.tlc;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TenantPagerAdapter extends FragmentPagerAdapter
{
    private final Fragment[] fragments = new Fragment[3];
    private static final String[] PAGE_TITLES = {"Events", "Tasks", "Chat"};

    public TenantPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);

//        fragments[0] = new PagerListFragment(R.layout.fragment_event_list);
//        fragments[1] = new PagerListFragment(R.layout.fragment_task_list);
//        fragments[2] = new PagerListFragment(R.layout.fragment_task_list);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return PAGE_TITLES[position];
    }

}