package com.csa.oswego.tlc.task;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Task implements Parcelable
{
    public final String title;
    public final String description;
    public final String imageUrl;
    public Bitmap image = null;

    public final long id;

    public Task(String title, String description, String imageUrl, long id)
    {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.id = id;
    }

    public Task(Parcel in)
    {
        id = in.readLong();
        title = in.readString();
        description = in.readString();
        imageUrl = in.readString();

        if (in.dataAvail() > 0)
        {
            image = in.readParcelable(Bitmap.class.getClassLoader());
        }
    }

    public static final Creator<Task> CREATOR = new Creator<Task>()
    {
        @Override
        public Task createFromParcel(Parcel in)
        {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size)
        {
            return new Task[size];
        }
    };

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(imageUrl);

        if (image != null)
        {
            image.writeToParcel(dest, flags);
        }
    }
}