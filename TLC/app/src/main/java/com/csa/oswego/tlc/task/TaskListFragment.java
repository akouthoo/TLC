package com.csa.oswego.tlc.task;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TaskListFragment extends ListFragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        final ArrayList<Task> tasks = new ArrayList<>();
        String url = "http://i.imgur.com/wq2CU0k.png";

        for (int i = 0; i < 10; i++)
        {
            String title = String.format("Task %d", i);
            String description = String.format("Description of Item %d", i);
            tasks.add(new Task(title, description, url, i));
        }

        setListAdapter(new TaskAdapter(getActivity(), tasks));

        return view;
    }
}