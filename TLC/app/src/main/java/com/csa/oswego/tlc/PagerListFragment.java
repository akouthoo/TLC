package com.csa.oswego.tlc;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PagerListFragment extends Fragment
{
    private final int fragmentLayoutId;

    private final View.OnClickListener onClickListener;
    private final int addButtonId;

    private View view;

    public PagerListFragment(int fragmentLayoutId, View.OnClickListener onClickListener, int addButtonId)
    {
        this.fragmentLayoutId = fragmentLayoutId;
        this.onClickListener = onClickListener;
        this.addButtonId = addButtonId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(fragmentLayoutId, container, false);
            view.findViewById(addButtonId).setOnClickListener(onClickListener);
        } catch (InflateException e) {
            // Already exists
        }
        return view;
    }
}