package com.csa.oswego.tlc.event;

import android.os.Parcel;
import android.os.Parcelable;
import org.joda.time.DateTime;

public class Event implements Parcelable
{
    public final String title;
    public final String description;
    public final DateTime date;

    public final long id;

    public Event(long id, String title, String description, DateTime date)
    {
        this.title = title;
        this.description = description;
        this.date = date;
        this.id = id;
    }

    public Event(Parcel in)
    {
        id = in.readLong();
        date = new DateTime(in.readLong());
        title = in.readString();
        description = in.readString();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>()
    {
        @Override
        public Event createFromParcel(Parcel in)
        {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size)
        {
            return new Event[size];
        }
    };

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeLong(date.getMillis());
        dest.writeString(title);
        dest.writeString(description);
    }
}