package com.csa.oswego.tlc.task;


import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.csa.oswego.tlc.util.Consumer;
import com.csa.oswego.tlc.util.DownloadImageTask;
import com.csa.oswego.tlc.R;

public class TaskView extends RelativeLayout implements Consumer<Task>
{
    private TextView mTitleTextView;
    private TextView mDescriptionTextView;
    private ImageView mImageView;

    public static TaskView inflate(ViewGroup parent) {
        return (TaskView)LayoutInflater.from(parent.getContext())
                .inflate(R.layout.taskview, parent, false);
    }

    public TaskView(Context context) {
        this(context, null, 0);
    }

    public TaskView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TaskView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        LayoutInflater.from(context).inflate(R.layout.task_list_item, this, true);
        setupChildren();
    }

    private void setupChildren() {
        mTitleTextView = (TextView) findViewById(R.id.task_title_text);
        mDescriptionTextView = (TextView) findViewById(R.id.task_desc_text);
        mImageView = (ImageView) findViewById(R.id.task_img);
        mImageView.setAdjustViewBounds(true);
    }

    @Override
    public void consume(final Task task)
    {
        mTitleTextView.setText(task.title);
        mDescriptionTextView.setText(task.description);

        Consumer<Bitmap> bitmapConsumer = new Consumer<Bitmap>()
        {
            @Override
            public void consume(Bitmap bitmap)
            {
                mImageView.setImageBitmap(bitmap);
                task.image = bitmap;
            }
        };

        new DownloadImageTask(bitmapConsumer).execute(task.imageUrl);
    }
}