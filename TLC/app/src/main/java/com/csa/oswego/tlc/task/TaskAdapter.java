package com.csa.oswego.tlc.task;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

public class TaskAdapter extends ArrayAdapter<Task>
{
    public TaskAdapter(Context context, List<Task> tasks)
    {
        super(context, 0, tasks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        TaskView taskView = (TaskView) convertView;
        if (taskView == null)
        {
            taskView = TaskView.inflate(parent);
        }

        taskView.consume(getItem(position));
        return taskView;
    }
}
