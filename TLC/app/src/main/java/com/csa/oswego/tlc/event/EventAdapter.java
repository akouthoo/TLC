package com.csa.oswego.tlc.event;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

public class EventAdapter extends ArrayAdapter<Event>
{
    public EventAdapter(Context context, List<Event> Events)
    {
        super(context, 0, Events);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        EventView eventView = (EventView) convertView;
        if (eventView == null)
        {
            eventView = EventView.inflate(parent);
        }

        eventView.consume(getItem(position));
        return eventView;
    }
}