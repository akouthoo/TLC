package com.csa.oswego.tlc.event;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.csa.oswego.tlc.util.Consumer;
import com.csa.oswego.tlc.R;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class EventView extends RelativeLayout implements Consumer<Event>
{
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("MM/dd/yyyy");

    private TextView mTitleTextView;
    private TextView mDescriptionTextView;
    private TextView mDateTimeTextView;

    public static EventView inflate(ViewGroup parent) {
        return (EventView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.eventview, parent, false);
    }

    public EventView(Context context) {
        this(context, null, 0);
    }

    public EventView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EventView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        LayoutInflater.from(context).inflate(R.layout.event_list_item, this, true);
        setupChildren();
    }

    private void setupChildren() {
        mTitleTextView = (TextView) findViewById(R.id.event_title_text);
        mDescriptionTextView = (TextView) findViewById(R.id.event_desc_text);
        mDateTimeTextView = (TextView) findViewById(R.id.event_date_text);
    }

    @Override
    public void consume(Event event)
    {
        mTitleTextView.setText(event.title);
        mDescriptionTextView.setText(event.description);
        mDateTimeTextView.setText(event.date.toString(DATE_TIME_FORMATTER));
    }
}